import javax.swing.*;
import java.awt.event.*;

// A complete program with menu and toolbar
// Inherits from CompleteWeatherLog, just adds menu and toolbar
public class WeatherLogLoadSave extends CompleteWeatherLog {

  public WeatherLogLoadSave () {
    super ();
		// Abstract action to be used in menu and toolbar, does nothing at all
    AbstractAction load = new AbstractAction ("Load", new ImageIcon (getClass().getResource("icons/OpenDoc.gif"))) {
      public void actionPerformed (ActionEvent ae) {
      }
    };
		// This it the tooltip for this item
    load.putValue (AbstractAction.SHORT_DESCRIPTION, "Load an existing datafile into the weather log");

		// Save, again , does nothing
    AbstractAction save = new AbstractAction ("Save", new ImageIcon (getClass().getResource("icons/Save.gif"))) {
      public void actionPerformed (ActionEvent ae) {
      }
    };
    save.putValue (AbstractAction.SHORT_DESCRIPTION, "Save this weather log to file");

		// The exit action works :)
    AbstractAction exit = new AbstractAction ("Exit") {
      public void actionPerformed (ActionEvent ae) {
        System.exit (0);
      }
    };
    exit.putValue (AbstractAction.SHORT_DESCRIPTION, "Closes the Weather log program");

		// Action object for creating a new row
    AbstractAction newRow = new AbstractAction ("New row", new ImageIcon (getClass().getResource("icons/NewRow.gif"))) {
      public void actionPerformed (ActionEvent ae) {
				// Inserts a new row into the weather model, unknow weather but the timestamp is now
				// User must edit the weather details
        wm.addRow (new WeatherItem (0, "", new java.util.Date()));
      }
    };
    newRow.putValue (AbstractAction.SHORT_DESCRIPTION, "Add a new row/line to this weather log");

		// Create the menu bar
    JMenuBar menu = new JMenuBar ();
		// The file menu, with load, save and exit
    JMenu file = new JMenu ("File");
    menu.add (file);
    file.add (load);
    file.add (save);
    file.addSeparator ();
    file.add (exit);

		// The edit menu, with new row
    JMenu edit = new JMenu ("Edit");
    edit.add (newRow);
    menu.add (edit);
    setJMenuBar (menu);

		// Create a toolbar with new row, load and save
    JToolBar toolbar = new JToolBar ();
    toolbar.add (newRow);
    toolbar.addSeparator ();
    toolbar.add (load);
    toolbar.add (save);
    getContentPane().add (toolbar, java.awt.BorderLayout.NORTH);

		// validate and repaint is needed to force a repaint of the layout 
		// after it has changed since first shown
    validate ();
    repaint ();
    setDefaultCloseOperation (EXIT_ON_CLOSE);
  }

  public static void main (String args[]) {
    WeatherLogLoadSave bwl = new WeatherLogLoadSave ();
  }
}