import javax.swing.*;
import javax.swing.table.*;

public class AdvancedWeatherLog extends JFrame {
  JTable weatherTable = null;
  WeatherModel wm = new WeatherModel ();

  public AdvancedWeatherLog () {
    super ("Weather log");
    wm.addRow (new WeatherItem (WeatherItem.SUNSHINE, "A bright sunny day", new java.util.Date()));
    wm.addRow (new WeatherItem (WeatherItem.CLOUDY_RAIN, "Evening rainstorm", new java.util.Date()));

    weatherTable = new JTable (wm);

    // Set up a drop down box as an editor
		JComboBox weatherTypes = new JComboBox (WeatherItem.getDescriptions());
		// Setting a different renderer for the drop down box
    weatherTypes.setRenderer (new WeatherDropDownRenderer ());
		// Fetching the weather type column from the table
    TableColumn typeColumn = weatherTable.getColumnModel().getColumn (0);
		// Setting the drop down box as the cell editor
    typeColumn.setCellEditor (new DefaultCellEditor(weatherTypes));
		// Changing the renderer for the cell
    typeColumn.setCellRenderer (new WeatherTypeRenderer());
    getContentPane().add (new JScrollPane (weatherTable));
    pack ();
    setVisible (true);
		setDefaultCloseOperation (EXIT_ON_CLOSE);
  }

  public static void main (String args[]) {
    AdvancedWeatherLog bwl = new AdvancedWeatherLog ();
  }
}