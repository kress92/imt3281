// Class used to store information about the weather at a given time
public class WeatherItem {
	// Consts for each weather type
  public static final int SUNSHINE = 1;
  public static final int LIGHT_CLOUDS = 2;
  public static final int PARTLY_CLOUDY = 3;
  public static final int CLOUDY = 4;
  public static final int PARTLY_CLOUDY_RAIN = 5;
  public static final int PARTLY_CLOUDY_RAIN_THUNDER = 6;
  public static final int PARTLY_CLOUDY_RAIN1 = 7;
  public static final int PARTLY_CLOUDY_RAIN2 = 8;
  public static final int CLOUDY_RAIN = 9;
  public static final int CLOUDY_HEAVY_RAIN = 10;
  public static final int CLOUDY_RAIN_THUNDER = 11;
  public static final int CLOUDY_RAIN1 = 12;
  public static final int CLOUDY_SNOW = 13;
  public static final int CLOUDY_RAIN1_THUNDER = 14;
  public static final int FOGGY = 15;
  public static final int MIDNIGHT_SUN = 16;
  public static final int MIDNIGHT_SUN_CLOUDY = 17;
  public static final int MIDNIGHT_SUN_CLOUDY_RAIN = 18;
  public static final int MIDNIGHT_SUN_CLOUDY_SNOW = 19;

	// 0 was not defined, so we call it unknown weather
  public static final String weatherDescriptions[] = {"Unknown weather", "Sunshine", "Lightly cloudy", "Partly cloudy", "Cloudy", "Partly cloudy with light rain", 
                                                      "Partly cloudy with thunder", "Partly cloudy with rain", "Partly cloudy with more rain", 
                                                      "Cloudy with light rain", "Cloudy with heavy rain", "Rain and thunder", "Cloudy with rain", 
                                                      "Clody with snow", "Thunderstorms", "Fog", "Sun with the sun below the horizon", 
                                                      "Sun below the horizon, cloudy", "Sun below the horizon, rain", "Sun below the horizon, snow" };

  private int weatherType;
  private String description;
  private java.util.Date timestamp;

	// Constructor receiving the type, description and time of observation
  public WeatherItem (int type, String desc, java.util.Date when) {
    weatherType = type;
    description = desc;
    timestamp = when;
  }

	// Return all the descriptions
  public static String[] getDescriptions () {
    return weatherDescriptions;
  }

	// Return the weather type identifier
  public int getWeatherType () {
    return weatherType;
  }

	// Return the description of this objects weather
  public String getWeatherDescription () {
    return description;
  }

	// Return the time of this observation
  public java.util.Date getDateTime () {
    return timestamp;
  }

	// Get the name (corresponding to the type) of this weater observation
  public String getWeatherName () {
    return weatherDescriptions[weatherType];
  }

	// Set the weather type for this observation (given the identifier)
  public void setWeatherType (int type) {
    weatherType = type;
  }

	// Set the weather type for this observation (given the name)
  public void setWeatherType (String type) {
    for (int i=0; i<weatherDescriptions.length; i++)
      if (type.equals (weatherDescriptions[i])) {
        setWeatherType (i);
        break;
      }
  }

	// Set the weather description
  public void setWeatherDescription (String desc) {
    description = desc;
  }

	// Set the time of this observation
  public void setDateTime (java.util.Date date) {
    timestamp = date;
  }
}