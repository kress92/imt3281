/**
 * Contains all source code for the JTable examples. 
 * For more information @see http://docs.oracle.com/javase/tutorial/uiswing/components/table.html
 * Order of the examples : 
 * 		BasicWeatherLog
 * 		ModeledWeatherLog
 * 			WeatherModel
 * 			WeatherItem
 * 		WeatherLog
 * 		ExtendedWeatherLog
 * 			WeatherTypeRenderer
 * 		AdvancedWeatherLog
 * 			WeatherDropDownRenderer
 * 		CompleteWeaterLog
 * 		WeatherLogLoadSave
 * 
 */
/**
 * @author oivindk
 *
 */
