import javax.swing.*;
import javax.swing.table.*;

public class WeatherLog extends JFrame {
  JTable weatherTable = null;
  WeatherModel wm = new WeatherModel ();

  public WeatherLog () {
    super ("Weather log");
    wm.addRow (new WeatherItem (1, "A bright sunny day", new java.util.Date()));
    wm.addRow (new WeatherItem (9, "Evening rainstorm", new java.util.Date()));

    weatherTable = new JTable (wm);

		// We want a different editor for the weather type
		// Filling a drop down box with the names of the the weather types
    JComboBox weatherTypes = new JComboBox (WeatherItem.getDescriptions());
		// Getting the weather type column
    TableColumn typeColumn = weatherTable.getColumnModel().getColumn (0);
		// Setting up a different editor for this column
    typeColumn.setCellEditor (new DefaultCellEditor(weatherTypes));
    getContentPane().add (new JScrollPane (weatherTable));
    pack ();
    setVisible (true);
		setDefaultCloseOperation (EXIT_ON_CLOSE);
  }

  public static void main (String args[]) {
    WeatherLog bwl = new WeatherLog ();
  }
}