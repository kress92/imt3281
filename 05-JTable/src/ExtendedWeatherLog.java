import javax.swing.*;
import javax.swing.table.*;

public class ExtendedWeatherLog extends JFrame {
  JTable weatherTable = null;
  WeatherModel wm = new WeatherModel ();

  public ExtendedWeatherLog () {
    super ("Weather log");
    wm.addRow (new WeatherItem (WeatherItem.SUNSHINE, "A bright sunny day", new java.util.Date()));
    wm.addRow (new WeatherItem (WeatherItem.CLOUDY_RAIN, "Evening rainstorm", new java.util.Date()));

    weatherTable = new JTable (wm);

		// Using a drop down box as editor
    JComboBox weatherTypes = new JComboBox (WeatherItem.getDescriptions());
		// Fetch the column
    TableColumn typeColumn = weatherTable.getColumnModel().getColumn (0);
		// Setting the editor
    typeColumn.setCellEditor (new DefaultCellEditor(weatherTypes));
		// And setting a different renderer for the column
		// This is the object that will be used to "draw" the contents of these cells
    typeColumn.setCellRenderer (new WeatherTypeRenderer());
    getContentPane().add (new JScrollPane (weatherTable));
    pack ();
    setVisible (true);
		setDefaultCloseOperation (EXIT_ON_CLOSE);
  }

  public static void main (String args[]) {
    ExtendedWeatherLog bwl = new ExtendedWeatherLog ();
  }
}