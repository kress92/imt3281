import javax.swing.*;

public class ModeledWeatherLog extends JFrame {
	// We have a table
  JTable weatherTable = null;
	// and a model to hold our data
  WeatherModel wm = new WeatherModel ();

  public ModeledWeatherLog () {
    super ("Weather log");
		// Adding data to the model
    wm.addRow (new WeatherItem (1, "A bright sunny day", new java.util.Date()));
    wm.addRow (new WeatherItem (9, "Evening rainstorm", new java.util.Date()));

		// Create the table based on the model
    weatherTable = new JTable (wm);
		// Adding it in a scrollpane
    getContentPane().add (new JScrollPane (weatherTable));
    pack ();
    setVisible (true);
		setDefaultCloseOperation (EXIT_ON_CLOSE);
  }

  public static void main (String args[]) {
    ModeledWeatherLog bwl = new ModeledWeatherLog ();
  }
}