import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

// Using a label as a starting point, ListCellRenderer must be implementet to act
// as such
public class WeatherDropDownRenderer extends JLabel implements ListCellRenderer {
	// A weather item object, needed to convert between textual and nummeric representation
	// of the weather
  WeatherItem wi = null;

  public WeatherDropDownRenderer () {
    setOpaque(true); //MUST do this for background to show up.
    setHorizontalAlignment(CENTER);	// The icon is to be centered in the label
    setVerticalAlignment(CENTER);
		// Just initialize the weatherItem object, will be changed as needed
    wi = new WeatherItem (0, "", new java.util.Date());
  }

  // This method is called every time a cell in the drop down box is to be rendered
	// Receives a reference to the actual drop down box, the value to be rendered
	// the index in the table and wether the item is focused or not
	public Component getListCellRendererComponent(JList list, Object value,
                                                int index, boolean isSelected,
                                                boolean cellHasFocus) {
		// Set the background to the lists background depending on the selection status
    if (isSelected) {
      setBackground(list.getSelectionBackground());
      setForeground(list.getSelectionForeground());
    } else {
      setBackground(list.getBackground());
      setForeground(list.getForeground());
    }

		// No text, just an image
    setText ("");
		// If unknown weather type, then use no icon
    if (((String)value).equals(WeatherItem.getDescriptions()[0]))
      setIcon (null);
    else {
			// We only get the textual description so set that in the weatherItem object
      wi.setWeatherType ((String)value);
			// Then we can get the number from the weatherItem object
      setIcon (new ImageIcon (getClass().getResource("images/"+wi.getWeatherType()+".gif")));
    }  
    return this;
  }
}