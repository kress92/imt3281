import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;

// Use a label as a starting point, must implement TableCellRenderer to be used as 
// a renderer of table cells
public class WeatherTypeRenderer extends JLabel implements TableCellRenderer {

  public WeatherTypeRenderer () {
    setOpaque(true); //MUST do this for background to show up.
    setHorizontalAlignment(CENTER); // Center the contents of the label
    setVerticalAlignment(CENTER);
  }

	// This is the method that is called to get the label to be drawn in the cell
	// Contains reference to the actual table, the value to be drawn, 
	// wether the element is selected and focused and the row and column of the element
  public Component getTableCellRendererComponent(
                          JTable table, Object value,
                          boolean isSelected, boolean hasFocus,
                          int row, int column) {
		// Setting the background of the label to the table background
		// depending on selection status
    if (isSelected) {
      setBackground (table.getSelectionBackground ());
    } else {
      setBackground (table.getBackground ());
    }
		// No text on the label, only an icon
    setText ("");
		// If value 0, that means unknown weather
    if (((Integer)value).toString().equals("0"))
      setIcon (null);
    else
			// The weather icons is numbered 1-19, corresponding to the weather identificators
      setIcon (new ImageIcon (getClass().getResource("images/"+((Integer)value).toString()+".gif")));
		// Return the label used to draw the table cell
    return this;
  }
}