import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Ex5MDI extends JFrame {
	// This is where all the child windows lives
	JDesktopPane desktopPane;
	
	public Ex5MDI () {
		super ("Many child windows");
		
		// Add a menu bar
		JMenuBar bar = new JMenuBar ();
		JMenu windowMenu = new JMenu ("Windows");
		windowMenu.setMnemonic ('W');
		JMenuItem addWindow = new JMenuItem ("Add window");
		addWindow.setMnemonic ('A');
		windowMenu.add (addWindow);
		bar.add (windowMenu);
		setJMenuBar (bar);
		




		// Create and add the desktop area to center
		desktopPane = new JDesktopPane ();
		add (desktopPane);
		






		// Add action listener to the menu option add child window
		addWindow.addActionListener (new ActionListener () {
			public void actionPerformed (ActionEvent ae) {
				addWindow ();
			}
		});
	}
	
	
	
	
	
	
	
	
	
	void addWindow () {
		// Create a new child window, title, resizable, closable, maximizable, iconifiable
		JInternalFrame jif = new JInternalFrame ("Internal frame", true, true, true, true);
				
		// MyPanel is a panel with a nice image :)
		MyPanel panel = new MyPanel();
		jif.add (panel);
		jif.pack ();
				
		// Fetch position of selected window
		Point p;
		if (desktopPane.getSelectedFrame()!=null)
			p = desktopPane.getSelectedFrame().getLocation();
		else
			p = new Point (-25, -25);
				



		// Add the child window to the desktop pane
		desktopPane.add (jif);

		// Position the new window down and to the right of the window below it
		jif.setLocation ((int)p.getX()+25, (int)p.getY()+25);

		// Then and only then set it to visible
		jif.setVisible (true);
	}
	
	public static void main (String args[]) {
		Ex5MDI ex = new Ex5MDI ();
		ex.setSize (1200, 900);
		ex.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		ex.setVisible (true);
	}
}






// Lazy lazy, we place the MyPanel class here to avoid extra files
class MyPanel extends JPanel {
	// Random number generator
	private static Random generator = new Random ();
	
	public MyPanel () {
		// The random number generator gets a number between zero and
		// the given value less one. We want it to be between 1 and the given number
		int randomNumber = generator.nextInt (5)+1;
		ImageIcon image = new ImageIcon (getClass().getResource (randomNumber+".jpg"));
		JLabel label = new JLabel (image);
		setLayout (new BorderLayout ());
		// Add the image in a scroll pane, so that when the windows gets resized we will still
		// be able to see the whole image
		add (new JScrollPane (label));
	}
}
