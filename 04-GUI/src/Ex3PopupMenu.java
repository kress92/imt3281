import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Ex3PopupMenu extends JFrame {
	private JRadioButtonMenuItem items[];
	private final static Color colorValues[] = { Color.LIGHT_GRAY, Color.BLUE, Color.RED, Color.YELLOW };
	private JPopupMenu popupMenu;
	
	public Ex3PopupMenu () {
		super ("Bring on the popups");
		
		ItemHandler handler = new ItemHandler ();
		String colors[] = { "Gray", "Blue", "Red", "Yellow" };
		ButtonGroup colorGroup = new ButtonGroup();
		// Create the popup menu object
		popupMenu = new JPopupMenu ();
		







		// Initialize the radio button menu items
		items = new JRadioButtonMenuItem[colors.length];
		for (int i=0; i<colors.length; i++) {
			items[i] = new JRadioButtonMenuItem(colors[i]);
			popupMenu.add (items[i]);
			colorGroup.add (items[i]);
			items[i].addActionListener (handler);
		}
		items[0].setSelected(true);
		
		setBackground (colorValues[0]);
		













		// Add mouse listener to the frame
		addMouseListener (new MouseAdapter () {
			// Mouse pressed might be a popup event
			public void mousePressed (MouseEvent me) {
				checkForTrigger (me);
			}
			
			// Mouse released might also be a popup event
			public void mouseReleased (MouseEvent me) {
				checkForTrigger (me);
			}
			
			// Check here if it is a popup trigger event
			private void checkForTrigger (MouseEvent me) {
				if (me.isPopupTrigger ()) {
					// Show the popup menu
					popupMenu.show (me.getComponent(), me.getX(), me.getY());
				}
			}
		});
	}















	// Listener to handle selections in the popup menu
	class ItemHandler implements ActionListener {
		public void actionPerformed (ActionEvent ae) {
			for (int i=0; i<items.length; i++) {
				// Check to see what radio button was pressed
				if (ae.getSource()==items[i]) {
					getContentPane().setBackground(colorValues[i]);
					return;
				}
			}
		}
	}	
	
	public static void main (String args[]) {
		Ex3PopupMenu ex = new Ex3PopupMenu ();
		ex.setSize (400, 300);
		ex.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		ex.setVisible (true);
	}
}
