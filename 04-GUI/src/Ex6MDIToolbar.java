import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Ex6MDIToolbar extends Ex5MDI {
	public Ex6MDIToolbar () {
		super ();
		// AbstractAction contains information used in both the menu and in the toolbar
		// This one will create a new window
		AbstractAction newWindow = new AbstractAction ("Add window", 
											new ImageIcon (getClass().getResource("new_window.png"))) {
			public void actionPerformed (ActionEvent ae) {
				addWindow();
			}
		};
		// Setting the mnemonic key used in menues
		newWindow.putValue(AbstractAction.MNEMONIC_KEY, (int)'A');
		// Same thing for the about item
		AbstractAction about = new AbstractAction ("About", 
											new ImageIcon (getClass().getResource("help.png"))) {
			public void actionPerformed (ActionEvent ae) {
				JOptionPane.showMessageDialog (Ex6MDIToolbar.this, "A great example of a MDI application", "About", JOptionPane.PLAIN_MESSAGE);
			}
		};
		about.putValue(AbstractAction.MNEMONIC_KEY, (int)'A');
		// And for the exit option
		AbstractAction exit = new AbstractAction ("Exit") {
			public void actionPerformed (ActionEvent ae) {
				System.exit (0);
			}
		};
		exit.putValue(AbstractAction.MNEMONIC_KEY, (int)'X');
		
		
		
		
		
		
		
		
		// Create a new menu bar
		JMenuBar bar = new JMenuBar ();
		JMenu fileMenu = new JMenu ("File");
		fileMenu.setMnemonic ('F');
		JMenu windowMenu = new JMenu ("Windows");
		windowMenu.setMnemonic ('W');
		JMenu helpMenu = new JMenu ("Help");
		helpMenu.setMnemonic ('H');
		







		// Adding the abstract action objects as menu items
		JMenuItem addWindowItem = new JMenuItem (newWindow);
		// Accelerator keys enable advanced users to access meny items without navigating the menu
		addWindowItem.setAccelerator (KeyStroke.getKeyStroke ('N', InputEvent.CTRL_DOWN_MASK));
		JMenuItem aboutItem = new JMenuItem (about);
		// The KeyEvent object defines all possible keystrokes, handy for function keys
		aboutItem.setAccelerator (KeyStroke.getKeyStroke (KeyEvent.VK_F1, 0));
		JMenuItem exitItem = new JMenuItem (exit);
		// Alt+F4 is close window on windows, but not on other OS's, probably shouldn't do this
		exitItem.setAccelerator (KeyStroke.getKeyStroke (KeyEvent.VK_F4, InputEvent.ALT_DOWN_MASK));
		fileMenu.add (exitItem);
		addWindowItem.setIcon (null);
		windowMenu.add (addWindowItem);
		aboutItem.setIcon (null);
		helpMenu.add (aboutItem);
		bar.add (fileMenu);
		bar.add (windowMenu);
		// Adding some glue, this puts the remaining menu items to the right
		bar.add (Box.createHorizontalGlue());
		bar.add (helpMenu);
		setJMenuBar (bar);
		





		
		JToolBar toolbar = new JToolBar ();
		toolbar.add (newWindow);
		// Same thing with the glue in a toolbar, puts the help/about item to the right
		toolbar.add (Box.createHorizontalGlue());
		toolbar.add (about);
		toolbar.addSeparator();
		add (toolbar, BorderLayout.NORTH);
	}
	
	public static void main (String args[]) {
		Ex6MDIToolbar ex = new Ex6MDIToolbar ();
		ex.setSize (1200, 900);
		ex.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		ex.setVisible (true);
	}
}








