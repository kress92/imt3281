import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Ex4LnF extends JFrame {
	private UIManager.LookAndFeelInfo looks[];
	private String lookNames[];
	private JRadioButton radio[];
	private ButtonGroup buttonGroup;
	private JButton button;
	private JLabel label;
	private JComboBox comboBox;
	
	public Ex4LnF () {
		super ("Look at my split personality");
		
		// Get information about available look and feels
		looks = UIManager.getInstalledLookAndFeels();
		lookNames = new String[looks.length];
		for (int i=0; i<looks.length; i++) {
			lookNames[i] = looks[i].getName();
		}

		
		
		
		
		
		// Panel to the North, containing label, button and combo box
		// just to show how the look and feel affects the application
		JPanel northPanel = new JPanel ();
		northPanel.setLayout (new GridLayout (3,1,0,5));
		
		// Create the text label
		label = new JLabel ("This is a "+lookNames[0]+" look-and-feel", SwingConstants.CENTER);
		northPanel.add (label);
		
		// Create the button 
		button = new JButton ("Just a button");
		northPanel.add (button);
		
		// Create the comboBox
		comboBox = new JComboBox (lookNames);
		northPanel.add (comboBox);
		add (northPanel, BorderLayout.NORTH);
		














		// Panel to the south, containing radio buttons used to change 
		// between the different look and feels
		JPanel southPanel = new JPanel ();
		// 0,3 means we don't know the number of rows, but use three columns
		southPanel.setLayout (new GridLayout (0,3));
		
		// Create radio buttons
		radio = new JRadioButton[looks.length];
		buttonGroup = new ButtonGroup();
		ItemHandler itemHandler = new ItemHandler ();
		for (int i=0; i<looks.length; i++) {
			radio[i] = new JRadioButton(lookNames[i]);
			southPanel.add (radio[i]);
			buttonGroup.add (radio[i]);
			radio[i].addItemListener (itemHandler);
		}
		radio[0].setSelected (true);
		add (southPanel, BorderLayout.SOUTH);
	}
		












		// Object of this class has its itemStateChanged method called each time
	// the user selects a look and feel radio button
	class ItemHandler implements ItemListener {
		public void itemStateChanged (ItemEvent ie) {
			// Find the selected look and feel
			for (int i=0; i<radio.length; i++) {
				if (ie.getSource()==radio[i]) {
					// Update the label
					label.setText (String.format ("This is a %s look-and-feel", lookNames[i]));
					// and combo box
					comboBox.setSelectedIndex (i);
					// Then change the actual look and feel
					changeLookAndFeel (i);
				}
			}
		}
	}
		









		// Method used to change the look and feel
	private void changeLookAndFeel (int idx) {
		try {
			// Tell the UI manager to change the look and feel
			// it needs the class name responsible for handling the lnf
			UIManager.setLookAndFeel (looks[idx].getClassName());
			// Call this method with the top of the component tree to be updated
			// Carefull, you can end up with different UI managers in different parts of
			// the application
			SwingUtilities.updateComponentTreeUI (this);
		} catch (Exception e) {
			e.printStackTrace ();
		}
	}
	
	public static void main (String args[]) {
		Ex4LnF ex = new Ex4LnF ();
		ex.setSize (1200, 900);
		ex.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		ex.setVisible (true);
	}
}
