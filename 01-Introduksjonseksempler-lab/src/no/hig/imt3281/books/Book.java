package no.hig.imt3281.books;

import javax.swing.JOptionPane;

/**
 * Objects of this class will hold information about a single book.
 * Information stored is title, author and price for the book. New objects of the Book class
 * can be created empty, with all information provided or by querying the user to provide the
 * information about the book to create.
 * Information about the book can be returned in pure text form or as a HTML formated text string.
 * @author okolloen
 *
 */
public class Book {
	private String title;
	private String author;
	private float price;
	
	/**
	 * Default blank constructor, initializes an empty Book object.
	 */
	public Book () {
		title = "";
		author = "";
		price = 0;
	}
	
	/**
	 * This constructor requires that information about title, author and price to be provided.
	 * Creates a book with the given title, author and price.
	 *  
	 * @param title a String containing the title of the book
	 * @param author a String containing the author of the book
	 * @param price a float value containing the price for the book
	 */
	public Book (String title, String author, float price) {
		this.title = title;
		this.author = author;
		this.price = price;
	}
	
	/**
	 * This method is used when the user is to be queried about the information about
	 * the book object to be created. The user will have to provide the title, author and price
	 * of the book and then a new book object will be created and returned with that information.
	 * If at any point the user presses cancel null will be returned.
	 *  
	 * @return a new object of Book with user provided information or null if the user presses
	 * cancel any time during the process of entering information.
	 */
	public static Book readFromUser () {
		String title="", author="", price="";
		while (title.equals("")) {		// No blank titles allowed
			title = JOptionPane.showInputDialog(Books.messages.getString("queryTitle"));
			if (title==null)			// If the user presses cancel, return null
				return null;
		}
		while (author.equals("")) {		// No blank authors allowed
			author = JOptionPane.showInputDialog(Books.messages.getString("queryAuthor"));
			if (author==null)			// If the user presses cancel, return null
				return null;
		}
		while (price.equals("")) {		// A book must have a price
			price = JOptionPane.showInputDialog(Books.messages.getString("queryPrice"));
			if (price==null)			// If the user presses cancel, return null
				return null;
		}
		// Return a Book object with the provided information, note: no checking on validity of price
		return new Book (title, author, Float.parseFloat(price));
	}
	
	/**
	 * Getter method for the title of the book.
	 * 
	 * @return a String with the title of the book
	 */
	public String getTitle () {
		return title;
	}
	
	/**
	 * Getter method for the author of the book.
	 * 
	 * @return a String with the author of the book
	 */
	public String getAuthor () {
		return author;
	}
	
	/**
	 * Getter method for the price of the book.
	 * 
	 * @return a float containing the price of the book
	 */
	public float getPrice () {
		return price;
	}

	/**
	 * Setter method for the title of the book.
	 * 
	 * @param title a String with the title of the book
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Setter method for the author of the book.
	 * 
	 * @param author a String with the author of the book
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * Setter method for the price of the book.
	 * 
	 * @param price a float value with the price of the book
	 */
	public void setPrice(float price) {
		this.price = price;
	}
	
	/**
	 * Overrides the toString method in the Object class, returns information about the 
	 * book in textual form.
	 * 
	 * @return a String with information about the book, usable for console output.
	 */
	public String toString () {
		return getTitle()+" "+Books.messages.getString("writtenBy")+" "+
				getAuthor()+" "+Books.messages.getString("cost")+" "+getPrice();
	}
	
	/**
	 * Returns information about the book in HTML form. The information is returned in a 
	 * form that is intended to be used as a row in a table. The title, author and price 
	 * is all td elements gathered together in a tr element.
	 * 
	 * @return a String with information about a book as a tr/td HTML collection
	 */
	public String toHTML () {
		return "<tr><td>"+getTitle()+"</td><td>"+getAuthor()+"</td><td>"+getPrice()+"</td></tr>";
	}
}