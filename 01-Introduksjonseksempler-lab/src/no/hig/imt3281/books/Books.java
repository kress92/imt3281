package no.hig.imt3281.books;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 * The Books class contains the toString and toHTML methods for a collection of books.
 * The class also contains a static reference to a message bundle that is initialized in
 * the main method of this class and is used by objects of the Book class.
 * 
 * The constructor of this class acts as a test for the Book and Books objects in that
 * it will allow the user to enter one or more books and then display the data in a 
 * variety of ways.
 * @author oivindk
 *
 */
public class Books {
	private Book books[] = new Book[10];
	/**
	 * The massages object is initialized in the main method of this object and 
	 * contains a reference to a resourceBundle responsible for the internationalization
	 * of this application. 
	 */
	public static ResourceBundle messages;

	/*
	 * This private method will run an integration test of the Books and Book classes.
	 * The user will be asked to fill in data for one or more books and then the 
	 * data is displayed in all the possible formats (text and HTML). 
	 */
	private void testBooks () {
		int currentBooks = 0;
		Book newBook = Book.readFromUser();
		while ((currentBooks<books.length-1)&&(newBook!=null)) {	// As long as the user keeps adding books
			books[currentBooks++] = newBook;
			int choice = JOptionPane.showConfirmDialog(null, messages.getString("addMore"));
			if (choice==JOptionPane.OK_OPTION)
				newBook = Book.readFromUser();
			else
				newBook = null;
		}
		System.out.println (this);											// Console display
		JOptionPane.showMessageDialog(null, this.toString());				// Display in dialog
		JOptionPane.showMessageDialog(null, new JLabel (this.toHTML()));	// Display HTML in dialog
	}
	
	/**
	 * The toString method returns data about the books as a string.
	 * Data about all registered books as well as the total cost of the books is returned.
	 * 
	 * @return a string containing the data.
	 */
	public String toString () {
		String s = "";
		float sum = 0;
		for (int i=0; i<books.length; i++) {
			if (books[i]!=null) {				// Only display data for actual books
				s += books[i]+"\n";
				sum += books[i].getPrice();
			}
		}
		// Return data for all books and total cost
		return s+"\t"+messages.getString("totalCost")+": "+sum;
	}
	
	/**
	 * the toHTML method returns data about the books as an HTML formated string.
	 * Data about each books is formated as a table row and the total cost is added 
	 * as a separate row. The title, author and price is columns in the returned rows.
	 * 
	 * @return a string containing the HTML formated data.
	 */
	public String toHTML () {
		String s = "<html><body><table><tr><td><b>"+messages.getString("title")+
				"</b></td><td><b>"+messages.getString("author")+
				"</b></td><td><b>"+messages.getString("price")+"</b></td></tr>\n";
		float sum = 0;
		for (int i=0; i<books.length; i++) {
			if (books[i]!=null) {
				s += books[i].toHTML()+"\n";
				sum += books[i].getPrice();
			}
		}
		return s+"<tr><td colspan='2' align='right'>"+messages.getString("totalCost")+
				"</td><td>"+sum+"</td></tr></table></body></html>";
	}
	
	/**
	 * The main method initializes the resource bundle used for the application.
	 * The application can be started with zero, one or two parameters. If no parameters
	 * are given the default locale will be used. With one parameter the language will be set,
	 * and with two parameters the country and language will be set.
	 *  
	 * @param args zero, one or two strings determining what country/language to use
	 */
	public static void main (String args[]) {		
		if (args.length==0) {
			messages = ResourceBundle.getBundle("i18n.introduction");
		} else if (args.length==1) {
			// Language
			messages = ResourceBundle.getBundle("introduction", new Locale(args[0]));
		} else if (args.length==2) {
			// Language, country (given as country, language on command line. I.e. no ny
			messages = ResourceBundle.getBundle("introduction", new Locale(args[1], args[0]));
		}
		Books books = new Books();	// Create a new Books object
		books.testBooks();			// Run the test on the books object
	}
}