package no.hig.imt3281.books;

import static org.junit.Assert.*;

import java.util.ResourceBundle;

import org.junit.Test;

/**
 * Tests for the Book class.
 */
public class BookTest {

	@Test
	public void testBook() {	// Tests blank constructor and getter methods
		Book b = new Book ();
		assertEquals("Title should be blank", "", b.getTitle());
		assertEquals("Author should be blank", "", b.getAuthor());
		assertEquals("Price should be zero", 0, b.getPrice(), 0);
	}

	@Test
	public void testBookStringStringFloat() {	// Tests constructor with parameters and getter methods
		Book b = new Book ("Title", "Author", 12.45f);
		assertEquals("Title should be 'Title'", "Title", b.getTitle());
		assertEquals("Author should be 'Author'", "Author", b.getAuthor());
		assertEquals("Price should be '12.45'", 12.45f, b.getPrice(), 0);
	}

	@Test
	public void testSetTitle() {	// Test set title method
		Book b = new Book ();
		b.setTitle("Title");
		assertEquals("Title should be 'Title'", "Title", b.getTitle());
	}

	@Test
	public void testSetAuthor() {	// Test set author method
		Book b = new Book ();
		b.setAuthor("Author");
		assertEquals("Author should be 'Author'", "Author", b.getAuthor());
	}

	@Test
	public void testSetPrice() {	// Test set price method
		Book b = new Book ();
		b.setPrice(12.45f);
		assertEquals("Price should be '12.45'", 12.45f, b.getPrice(), 0);
	}

	@Test
	public void testToString() {	// Test toString method
		// Note, this is not a pure unit test as it relies on the Books object
		Book b = new Book ("Title", "Author", 12.45f);
		Books.messages = ResourceBundle.getBundle("i18n.introduction");
		String goal = "Title "+Books.messages.getString("writtenBy")+" Author "+
				Books.messages.getString("cost")+" 12.45";
		assertEquals(goal, b.toString());
	}

	@Test
	public void testToHTML() {		// Test the toHTML method
		Book b = new Book ("Title", "Author", 12.45f);
		String goal = "<tr><td>Title</td><td>Author</td><td>12.45</td></tr>";
		assertEquals(goal, b.toHTML());
	}
}
