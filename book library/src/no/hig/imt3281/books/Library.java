package no.hig.imt3281.books;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class Library {
	Book[] books = new Book[10];
	public static ResourceBundle messages;
	public static Locale currentLocale;
	
	public Library () {
		books[0] = new Book ("Java how to program", "Deitel & Deitel", 43.23f);
		books[1] = new Book("Java for programmers", "Deitel & Deitel", 23.45f);
		System.out.println(books[0]);
		System.out.println("=========");
		System.out.println(this);
	}
	
	public String toString() {
		String s = "";
		float sum = 0;
		for (int i=0; i<books.length; i++) {
			if (books[i]!=null) {
				s += books[i]+"\n";
				sum += books[i].getPrice();
			}
		}
		Object messageArguments[] = { s, new Float(sum) };
		// http://docs.oracle.com/javase/7/docs/api/java/text/MessageFormat.html
		MessageFormat formatter = new MessageFormat("");
		formatter.setLocale(currentLocale);
		formatter.applyPattern(messages.getString("summary"));
		return formatter.format(messageArguments);
	}
	
	public static void main(String[] args) {
		if (args.length==0) {
			currentLocale = Locale.getDefault();
		} else if (args.length==1) {
			// Language
			currentLocale = new Locale(args[0]);
		} else if (args.length==2) {
			// Language, country (given as COUNTRY, LANGUAGE on command line. I.e. no ny
			currentLocale = new Locale(args[1], args[0]);
		}
		messages = ResourceBundle.getBundle("i18n.library", currentLocale);
		new Library();
	}
}
