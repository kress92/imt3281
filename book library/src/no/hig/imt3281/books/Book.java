package no.hig.imt3281.books;

import java.text.NumberFormat;

public class Book {
	private String title;
	private String author;
	private float price;
	
	public Book () {
		title = "";
		author = "";
		price = 0;
	}

	public Book(String title, String author, float price) {
		super();
		this.title = title;
		this.author = author;
		this.price = price;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	public String toString() {
		NumberFormat currencyFormatter;
		currencyFormatter = NumberFormat.getCurrencyInstance(Library.currentLocale);

		return getTitle()+" "+Library.messages.getString("writtenBy")+" "+
				getAuthor()+" "+Library.messages.getString("cost")+" "+currencyFormatter.format(getPrice());
	}
}
