/**
 * This package contains all resource files used for internationalization.
 * Note that the I18N_en.properties and I18N.properties are copies of each other.
 * The reason for this is that since on norwegian computers the default language is no
 * and when forcing english as the language it searches for I18N_en.properties. 
 * When this is not found it falls back to the default language and searches for 
 * I18N_no.properties. Only then if that is not found will it search for I18N.properties.
 * 
 */
/**
 * @author oivindk
 *
 */
package i18n;