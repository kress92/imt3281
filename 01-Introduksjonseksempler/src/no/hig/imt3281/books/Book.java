package no.hig.imt3281.books;

public class Book {
	private String title;
	private String author;
	private float price;
	
	public Book () {
		title = "";
		author = "";
		price = 0;
	}
	
	public Book (String title, String author, float price) {
		this.title = title;
		this.author = author;
		this.price = price;
	}
	
	public String getTitle () {
		return title;
	}
	
	public String getAuthor () {
		return author;
	}
	
	public float getPrice () {
		return price;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	public String toString () {
		return getTitle()+" by "+getAuthor()+" is worth "+getPrice();
	}
}
