package no.hig.imt3281.I18N;

import java.util.*;
import java.text.*;

public class Info {
	public static void main (String args[]) {
		int nrStudents = 2500;
		Date now = new Date ();
		
		Locale currentLocale;
		ResourceBundle messages;
		if (args.length==2) // Spr�k, land
			currentLocale = new Locale (args[0], args[1]);
		else if (args.length==1) // spr�k
			currentLocale = new Locale (args[0]);
		else
			currentLocale = Locale.getDefault();
		System.out.println (currentLocale.getDisplayCountry());
		System.out.println (currentLocale.getDisplayLanguage());
		
		messages = ResourceBundle.getBundle("i18n.I18N", currentLocale);

		System.out.println(messages.getString("welcome"));
		
		// ===================================================================
		DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.DEFAULT, currentLocale);
		String dateOut = dateFormatter.format(now);
		System.out.println(dateOut + " DEFAULT");

		// -----------
		dateFormatter = DateFormat.getDateInstance(DateFormat.SHORT, currentLocale);
		dateOut = dateFormatter.format(now);
		System.out.println(dateOut + " SHORT");
		
		// -----------
		DateFormat timeFormatter = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocale);
		dateOut = timeFormatter.format(now);
		System.out.println(dateOut + " DEFAULT");
		
		// -----------
		timeFormatter = DateFormat.getTimeInstance(DateFormat.SHORT, currentLocale);
		dateOut = timeFormatter.format(now);
		System.out.println(dateOut + " SHORT");
		
		// -----------
		DateFormat dateTimeFormatter = DateFormat.getDateTimeInstance(DateFormat.FULL,
		DateFormat.FULL, currentLocale);
		dateOut = dateTimeFormatter.format(now);
		System.out.println(dateOut + " FULL, FULL");
		
		// ================================================================
		Double currency = new Double(1500.21);
		NumberFormat currencyFormatter;
		String currencyOut;
		currencyFormatter = NumberFormat.getCurrencyInstance(currentLocale);
		currencyOut = currencyFormatter.format(currency);
		System.out.println(currencyOut);
		
		// ================================================================
		System.out.println("Pr. "+now+" er det registrert "+nrStudents+" studenter ved HiG.");
		Object messageArguments[] = { new Integer(nrStudents), now };
		// http://docs.oracle.com/javase/7/docs/api/java/text/MessageFormat.html
		MessageFormat formatter = new MessageFormat("");
		formatter.setLocale(currentLocale);
		formatter.applyPattern(messages.getString("nrStudentsNow"));
		String output = formatter.format(messageArguments);
		System.out.println (output);
		
		// =====================================================================
		Collator myDefaultCollator = Collator.getInstance(currentLocale);
		String s1 = "A";
		String s2 = "�";
		System.out.println (s1+".compareTo("+s2+")="+s1.compareTo(s2));
		System.out.println ("collator.compare("+s1+","+s2+")="+myDefaultCollator.compare(s1, s2));
		System.out.println ("� is a letter : "+Character.isLetter ('�'));
	}
}