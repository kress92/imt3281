import java.awt.*;
import javax.swing.*;

/**
 * Creates a 400x400 frame with a title.
 * 
 * @author okolloen
 *
 */
public class Eks1 {
	public static void main (String args[]) {
		JFrame f = new JFrame ("Hello world");			// Create new titled frame
		f.setMinimumSize (new Dimension (400, 400));	// Set the dimensions
		f.setVisible (true);							// Show it on screen
	}
}