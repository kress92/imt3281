import java.awt.*;

import javax.swing.*;

/**
 * Creates a 400x400 frame by inheriting from JFrame
 * @author okolloen
 *
 */
@SuppressWarnings("serial")
public class Eks2 extends JFrame {

	/**
	 * Default constructor, sets up the frame
	 */
	public Eks2 () {
		super ("Hello world");						// Add the title
		setMinimumSize (new Dimension (400, 400));	// Set the dimension
		setVisible (true);							// Show the frame
	}
	
	public static void main (String args[]) {
		new Eks2 ();
	}
}