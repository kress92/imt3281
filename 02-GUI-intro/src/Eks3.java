import javax.swing.*;

/**
 * Creates a frame with a button.
 * 
 * @author okolloen
 *
 */
@SuppressWarnings("serial")
public class Eks3 extends JFrame {
	
	/**
	 * Default constructor, sets up the frame and add the button.
	 */
	public Eks3 () {
		super ("Hello world");					// Add title to the frame
		JButton b = new JButton ("Press here");	// Create a new button
		add (b);								// Add the button to the frame
		pack();									// Make just enough room for everything
		setVisible (true);						// Show the frame
	}
	
	public static void main (String args[]) {
		new Eks3 ();
	}
}