import java.awt.*;

import javax.swing.*;

/**
 * Create a 3x3 grid of textfields.
 * 
 * @author okolloen
 *
 */
@SuppressWarnings("serial")
public class Eks6 extends JFrame {
	private JTextField grid[] = new JTextField[9];
	
	/**
	 * Default constructor, creates the layout
	 */
	public Eks6 () {
		super ("mSudoku");						// Set the title of the frame
		setLayout (new GridLayout (3,3));		// Use a 3x3 grid as layout
		for (int i=0; i<grid.length; i++) {		// Loop through the array with 9 textfields
			grid[i] = new JTextField ("0");		// Create a new JTextField object
			add (grid[i]);						// Add it to the grid
		}
		pack();									// Make just enough room for all components
		setVisible (true);						// Show the window
	}
	
	public static void main (String args[]) {
		new Eks6 ();
	}
}