import java.awt.*;

import javax.swing.*;

/**
 * Create a window containing a layout for the game of Sudoku.
 * 
 * @author okolloen
 *
 */
@SuppressWarnings("serial")
public class Eks7 extends JFrame {
	private JTextField grid[] = new JTextField[9*9];

	/**
	 * Default constructor, creates the GUI
	 */
	public Eks7 () {
		super ("mSudoku");					// Set title of the frame
		setLayout (new GridLayout (3,3));	// The sudoku board consists of a 3x3 grid of 3x3 cells
		for (int i=0; i<9; i++) {			// Loop through the 3x3 grid
			add (createGrid(i));			// Add a panel of 3x3 cells to each position in the grid
		}
		pack();								// Create just enough room
		setVisible (true);					// Show the window
	}

	/*
	 * Note, no need to create a JavaDoc comment as private methods 
	 * will normally not have JavaDoc documentation generated.
	 * 
	 * This method creates a 3x3 panel containing a sub part of the sudoku board
	 * 
	 * @param id identifies what part of the larger 3x3 sudoku board this panel will occupy
	 */
	private JPanel createGrid (int id) {
		JPanel p = new JPanel ();			// Create a new panel
		p.setLayout (new GridLayout (3,3));	// Use a 3x3 grid to lay out components
		for (int i=0; i<9; i++) {			// Loop through the 9 cells of the layout
			JTextField t = new JTextField ("1", 2);			// Create a new textfield
			t.setHorizontalAlignment (JTextField.CENTER);	// Center the content of the textfield
			grid[id*9+i] = t;				// Add the textfield to the complete sudoku grid
			p.add (grid[id*9+i]);			// Add the textfield to the layout
		}
		p.setBorder (BorderFactory.createLineBorder (Color.black));	// Add a border around this part 
		return p;							// Return this panel
	}
	
	public static void main (String args[]) {
		new Eks7 ();
	}
}