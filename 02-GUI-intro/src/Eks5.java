import javax.swing.*;

import java.awt.*;

/**
 * Create a complex GUI using GridLayout for placing components.
 * 
 * @author okolloen
 *
 */
@SuppressWarnings("serial")
public class Eks5 extends JFrame {
	/**
	 * Default constructor, creates the GUI 
	 */
	public Eks5 () {
		super ("Hello world");						// Set title of frame
		setLayout (new GridLayout (1,3));			// Splits frame into three columns
		
		JButton ok = new JButton ("OK");			// Creates a button
		add (ok);									// Add it to the first column of the frame
		
		JPanel pCancel = new JPanel ();				// Creates a panel
		JButton cancel = new JButton ("Cancel");	// Creates a button
		pCancel.add (cancel);						// Add the button to the panel
		add (pCancel);								// Add the panel to the second column of the frame
		
		JPanel pTwo = new JPanel ();				// Create a panel
		pTwo.setLayout(new GridLayout (2,1));		// Split it into two rows
		JButton bOne = new JButton ("One");			// Creates a button
		JButton bTwo = new JButton ("Two");			// Creates another button
		pTwo.add (bOne);							// Add button to first row
		pTwo.add (bTwo);							// Add button to second row
		add (pTwo);									// Add the panel to the third column of the frame
		
		pack ();									// Makes just enough room 
		setVisible (true);							// Show the window
	}
	
	public static void main (String args[]) {
		new Eks5();
	}
}
