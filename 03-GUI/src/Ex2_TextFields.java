import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Ex2_TextFields extends JFrame {
	
	JTextField text1 = new JTextField ("Enter text and press enter"); 
	JPasswordField text2 = new JPasswordField (15);
	
	public Ex2_TextFields () {
		super ("Test av tekstfelter");
		setLayout (new FlowLayout ()); 
		TextFieldHandler tfh = new TextFieldHandler (); 
		
		add (text1);
		text1.addActionListener (tfh);
		add (text2);
		text2.addActionListener (tfh);
		pack();
		setVisible (true); 
	}
	
	class TextFieldHandler implements ActionListener { 
		
		public void actionPerformed (ActionEvent ae) {
			if (ae.getSource()==text1)
				JOptionPane.showMessageDialog (null, "Tekstfeltet har verdien : "+ae.getActionCommand ());
			else if (ae.getSource()==text2)
				JOptionPane.showMessageDialog (null, "Passorder er : "+ae.getActionCommand()); 
		}
	}
	
	public static void main (String args[]) { 
		new Ex2_TextFields ();
	} 
}