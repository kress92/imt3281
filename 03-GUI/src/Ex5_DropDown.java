import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class Ex5_DropDown extends JFrame { 
	private JComboBox imageSelector;
	private JLabel label;
	private static final String names[] = { "sunny.png", "partially_cloudy.png", "cloudy.png", "partially_cloudy_rain.png", "rainy.png", "heavy_rain.png" };
	private Icon images[] = {new ImageIcon (getClass().getResource ("symboler/"+names[0])), new ImageIcon (getClass().getResource ("symboler/"+names[1])), 
							 new ImageIcon (getClass().getResource ("symboler/"+names[2])), new ImageIcon (getClass().getResource ("symboler/"+names[3])), 
							 new ImageIcon (getClass().getResource ("symboler/"+names[4])), new ImageIcon (getClass().getResource ("symboler/"+names[5])) }; 

	public Ex5_DropDown () { 
		super ("Velg fra nedtrekkslisten");
		setLayout (new FlowLayout ());
		imageSelector = new JComboBox (names); 
		imageSelector.setMaximumRowCount (3); 
		imageSelector.addItemListener (new ImageSelectorListener ());
		add (imageSelector);
		label = new JLabel (images[0]);
		add (label); 
	}
	
	private class ImageSelectorListener implements ItemListener { 
		
		public void itemStateChanged (ItemEvent ie) {
			if (ie.getStateChange() == ItemEvent.SELECTED) 
				label.setIcon (images[imageSelector.getSelectedIndex()]);
		} 
	}
}