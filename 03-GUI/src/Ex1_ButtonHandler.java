import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Ex1_ButtonHandler extends JFrame {
	JLabel text;
	JButton button;
	String messages[] = { "Au", "Kutt ut", "Det holder nå", "Nå blir jeg sint", "Du ba om det" };
	int count = 0;

	public Ex1_ButtonHandler () {
		super ("Reagere på hendelser"); 
		button = new JButton ("Trykk på meg"); 
		text = new JLabel (" ");
		text.setFont (new Font ("Arial", Font.PLAIN, 32)); 
		add (button, BorderLayout.CENTER);
		button.addActionListener (new MyButtonAction ()); 
		add (text, BorderLayout.SOUTH);
		setMinimumSize (new Dimension (500, 100));
		setVisible (true); 
	}
	
	class MyButtonAction implements ActionListener { 
		
		public void actionPerformed (ActionEvent ae) {
			text.setText (messages[count]); 
			count++;
			if (count==messages.length)
				button.setEnabled (false); 
		}
	}
		
	public static void main (String args[]) { 
		new Ex1_ButtonHandler ();
	} 
}