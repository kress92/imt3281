import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Ex9_MouseAndKeys extends JFrame { 
	private String line1 = "";
	private String line2 = "";
	private String line3 = "";
	private JTextArea textArea; 
	
	public Ex9_MouseAndKeys () {
		super ("Test of mouse and keyboard"); 
		textArea = new JTextArea (10,15);
		textArea.setText ("Press any key on the keyboard...\nOr press any mouse keys"); 
		textArea.setEnabled (false);
		textArea.setDisabledTextColor (Color.BLACK);
		add (textArea);
		MyListener listener = new MyListener (); 
		addKeyListener (listener); 
		textArea.addMouseListener (listener);
	}
	
	private class MyListener extends MouseAdapter implements KeyListener {
		// MouseAdapter
		public void mouseClicked (MouseEvent me) {
			line1 = String.format ("Clicked %d time(s) at [%d, %d], ", me.getClickCount(), me.getX(), me.getY());
			if (me.isMetaDown())
				line2 = "width right mouse button"; 
			else if (me.isAltDown())
				line2 = "width middle mouse button"; 
			else
				line2 = "width left mouse button";
			textArea.setText (String.format ("%s\n%s", line1, line2));
		}
		
		// Key Listener
		public void keyPressed (KeyEvent event) {
			line1 = String.format ("Key pressed: %s", KeyEvent.getKeyText(event.getKeyCode())); 
			setLines2and3 (event);
		}
		
		public void keyReleased (KeyEvent event) {
			line1 = String.format ("Key released: %s", KeyEvent.getKeyText(event.getKeyCode())); 
			setLines2and3 (event);
		}
		
		public void keyTyped (KeyEvent event) {
			line1 = String.format ("Key types: %s", event.getKeyChar()); 
			setLines2and3 (event);
		}
		
		private void setLines2and3 (KeyEvent event) {
			line2 = String.format ("This key is %san action key", (event.isActionKey()?"":"not "));
			String tmp = KeyEvent.getKeyModifiersText (event.getModifiers());
			line3 = String.format ("Modifier keys pressed : %s", (tmp.equals("")?"none":tmp)); 
			textArea.setText (String.format ("%s\n%s\n%s", line1, line2, line3));
		} 
	}
}